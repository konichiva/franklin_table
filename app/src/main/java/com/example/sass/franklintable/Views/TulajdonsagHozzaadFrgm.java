package com.example.sass.franklintable.Views;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.sass.franklintable.R;
import com.example.sass.franklintable.dbhandlers.TulajdonsagDbHandler;
import com.example.sass.franklintable.entitys.Tulajdonsag;


public class TulajdonsagHozzaadFrgm extends Fragment {

    Button mentes;
    EditText tulajdonsagNeve, tulajdonsagLeiras;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tulajdonsag_hozzaad_frgm, parent, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mentes = getView().findViewById(R.id.tulajdonsag_mentes);

        mentes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tulajdonsagLeiras = getView().findViewById(R.id.tulajdonsagLeirás);
                String tulLeir= tulajdonsagLeiras.getText().toString();
                tulajdonsagNeve=getView().findViewById(R.id.tulajdonsagNeve);
                String tulNev= tulajdonsagNeve.getText().toString();
                Tulajdonsag tul = new Tulajdonsag();
                tul.setTulajdonsagLeiras(tulLeir);
                tul.setTulajdonsagNev(tulNev);
                TulajdonsagDbHandler db = new TulajdonsagDbHandler(getActivity().getApplicationContext());
                db.addTulajdonsag(tul);
            }
        });



    }
}
