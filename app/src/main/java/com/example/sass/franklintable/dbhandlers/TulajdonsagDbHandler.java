package com.example.sass.franklintable.dbhandlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sass.franklintable.entitys.Tulajdonsag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SASS on 2017. 11. 19..
 */

public class TulajdonsagDbHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "FRANKLIN_TABLE";
    private static final String TABLE_TULAJDONSAGOK = "tulajdonsagok";

    private static final String KEY_ID= "id";
    private static final String TULAJDONSAG_NEV="tulajdonsagNev";
    private static final String TULAJDONSAG_LEIRAS="tulajdonosLeiras";


    public TulajdonsagDbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTulajdonsagTable ="CREATE TABLE "+ TABLE_TULAJDONSAGOK +
                "("+KEY_ID+ " INTEGER PRIMARY KEY," + TULAJDONSAG_NEV +
                " TEXT," + TULAJDONSAG_LEIRAS +" TEXT" + ")";
        db.execSQL(createTulajdonsagTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TULAJDONSAGOK);
        onCreate(db);
    }

    public void addTulajdonsag(Tulajdonsag tulajdonsag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TULAJDONSAG_NEV, tulajdonsag.getTulajdonsagNev());
        values.put(TULAJDONSAG_LEIRAS,tulajdonsag.getTulajdonsagLeiras());
        db.insert(TABLE_TULAJDONSAGOK, null, values);
        db.close(); // Closing database connection
    }

    public Tulajdonsag getTulajdonsag(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_TULAJDONSAGOK, new String[] { KEY_ID,
                        TULAJDONSAG_NEV, TULAJDONSAG_LEIRAS }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Tulajdonsag tl = new Tulajdonsag(Long.parseLong(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        return tl;
    }

    public ArrayList<Tulajdonsag> getAllTulajdonsag() {
        ArrayList<Tulajdonsag> tlList = new ArrayList<Tulajdonsag>();
        String selectQuery = "SELECT * FROM " + TABLE_TULAJDONSAGOK;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Tulajdonsag tl = new Tulajdonsag();
                tl.setId(Long.parseLong(cursor.getString(0)));
                tl.setTulajdonsagNev(cursor.getString(1));
                tl.setTulajdonsagLeiras(cursor.getString(2));

                tlList.add(tl);
            } while (cursor.moveToNext());
        }
        return tlList;
    }


    public int getTulajdonsagSzama() {
        String countQuery = "SELECT * FROM " + TABLE_TULAJDONSAGOK;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }

    public int updateTulajdonsag(Tulajdonsag tulajdonsag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TULAJDONSAG_NEV, tulajdonsag.getTulajdonsagNev());
        values.put(TULAJDONSAG_LEIRAS, tulajdonsag.getTulajdonsagLeiras());
        return db.update(TABLE_TULAJDONSAGOK, values, KEY_ID + " = ?",
                new String[]{String.valueOf(tulajdonsag.getId())});
    }

    public void deleteTulajdonsag(Tulajdonsag tulajdonsag) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TULAJDONSAGOK, KEY_ID + " = ?",
                new String[] { String.valueOf(tulajdonsag.getId()) });
        db.close();
    }



}
