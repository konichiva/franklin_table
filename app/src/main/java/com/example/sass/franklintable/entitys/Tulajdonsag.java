package com.example.sass.franklintable.entitys;

/**
 * Created by SASS on 2017. 11. 19..
 */

public class Tulajdonsag {
    private Long id;
    private String tulajdonsagNev;
    private String tulajdonsagLeiras;

    public Tulajdonsag(Long id, String tulajdonsagNev, String tulajdonsagLeiras) {
        this.id = id;
        this.tulajdonsagNev = tulajdonsagNev;
        this.tulajdonsagLeiras = tulajdonsagLeiras;
    }

    public Tulajdonsag() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTulajdonsagNev() {
        return tulajdonsagNev;
    }

    public void setTulajdonsagNev(String tulajdonsagNev) {
        this.tulajdonsagNev = tulajdonsagNev;
    }

    public String getTulajdonsagLeiras() {
        return tulajdonsagLeiras;
    }

    public void setTulajdonsagLeiras(String tulajdonsagLeiras) {
        this.tulajdonsagLeiras = tulajdonsagLeiras;
    }
}
