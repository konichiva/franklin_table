package com.example.sass.franklintable.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sass.franklintable.R;
import com.example.sass.franklintable.dbhandlers.TulajdonsagDbHandler;
import com.example.sass.franklintable.entitys.Tulajdonsag;

import java.util.ArrayList;

/**
 * Created by SASS on 2017. 12. 11..
 */

public class TulajdonsagCustomAdapter extends BaseAdapter implements ListAdapter {

    private ArrayList<Tulajdonsag> list = new ArrayList<Tulajdonsag>();
    private Context context;
    public TulajdonsagCustomAdapter(ArrayList<Tulajdonsag> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.tulajdonsag_row_item, null);
        }
        TextView listItemText = (TextView)view.findViewById(R.id.list_item_string);
        listItemText.setText(list.get(position).getTulajdonsagNev());

        //Handle buttons and add onClickListeners
        Button deleteBtn = (Button)view.findViewById(R.id.delete_btn);

        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                 TulajdonsagDbHandler dbHandler = new TulajdonsagDbHandler(context);
                dbHandler.deleteTulajdonsag(list.get(position));
                list.remove(position); //or some other task
                notifyDataSetChanged();
            }
        });

        return view;
    }
}

