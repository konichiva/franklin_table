package com.example.sass.franklintable.Views;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.sass.franklintable.Adapters.TulajdonsagCustomAdapter;
import com.example.sass.franklintable.R;
import com.example.sass.franklintable.dbhandlers.TulajdonsagDbHandler;
import com.example.sass.franklintable.entitys.Tulajdonsag;

import java.util.ArrayList;


public class TulajdonsagListFrgm extends Fragment {


    ListView tulajdonsagList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tulajdonsag_list_frgm, parent, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TulajdonsagDbHandler db = new TulajdonsagDbHandler(getActivity().getApplicationContext());
        ArrayList<Tulajdonsag> list ;
        list = db.getAllTulajdonsag();
        TulajdonsagCustomAdapter adapter = new TulajdonsagCustomAdapter(list, getActivity().getApplicationContext());
        tulajdonsagList = (ListView)getView().findViewById(R.id.tulajdonsag_list);
        tulajdonsagList.setAdapter(adapter);
    }
}
